terraform {
  backend "http"{}
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = var.region
}

resource "aws_vpc" "petvpc" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "Petclinic VPC"
  }
}

# Create two subnets, each in separate AZ
# public for web app
resource "aws_subnet" "petsubnet_public" {
  vpc_id                  = aws_vpc.petvpc.id
  cidr_block              = var.subnet_cidr_blocks[0]
  availability_zone_id    = var.region_az[0]
  map_public_ip_on_launch = true

  tags = {
    Name = "Petclinic public subnet"
  }
}

# private for database. it is not to be accessed by any service other than web app
resource "aws_subnet" "petsubnet_private" {
  vpc_id               = aws_vpc.petvpc.id
  cidr_block           = var.subnet_cidr_blocks[1]
  availability_zone_id = var.region_az[1]
  tags = {
    Name = "Petclinic private subnet"
  }
}

# Define an internet gateway which would route the traffic outside the webapp vpc to internet
resource "aws_internet_gateway" "petgw" {
  vpc_id = aws_vpc.petvpc.id
  tags = {
    Name = "Petclinic internet gateway"
  }
}


# Define a route table with rules for how to route traffic
# traffic to cidr block is routed in localhost
# traffic outside of the cidr block should be routed through internet gateway
resource "aws_default_route_table" "petroute_public" {
  #   vpc_id = aws_vpc.petvpc.id
  default_route_table_id = aws_vpc.petvpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.petgw.id
  }

  tags = {
    Name = "Petclinic routing table"
  }
}
 

# private traffic is not routed outside of the VPC
resource "aws_route_table" "petroute_private" {
  vpc_id = aws_vpc.petvpc.id

  tags = {
    Name = "Petclinic Route table private"
  }

}


resource "aws_route_table_association" "petroute-priv-assoc" {
  subnet_id      = aws_subnet.petsubnet_private.id
  route_table_id = aws_route_table.petroute_private.id
}
