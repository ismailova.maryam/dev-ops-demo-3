locals {
  jar_name = one(fileset("${path.cwd}/target/", "*.jar"))
}

resource "aws_instance" "web" {
  ami                    = var.java_ami
  instance_type          = "t2.micro"
  key_name               = "terraform-deploy-key"
  vpc_security_group_ids = [aws_security_group.petsg-app.id]
  subnet_id              = aws_subnet.petsubnet_public.id

  provisioner "file" {
    source      = "${path.cwd}/target/${local.jar_name}"
    destination = "/home/ubuntu/${local.jar_name}"
  }
  provisioner "file" {
    # FOR RDS
    # content = templatefile("${path.cwd}/petclinic.service", { MYSQL_USER = "${var.MYSQL_USER}", MYSQL_PASSWORD = "${var.MYSQL_PASSWORD}", MYSQL_URL = "jdbc:mysql://${aws_db_instance.db.address}/petclinic", JAR_NAME = "${local.jar_name}" })
    content = templatefile("${path.cwd}/petclinic.service", { MYSQL_USER="${var.MYSQL_USER}", MYSQL_PASSWORD="${var.MYSQL_PASSWORD}", MYSQL_URL="jdbc:mysql://${aws_instance.db.private_ip}/petclinic", JAR_NAME="${local.jar_name}" })
    destination = "/home/ubuntu/petclinic.service"
  }

  provisioner "remote-exec" {
    # FOR RDS
    # mysql -h ${aws_db_instance.db.address} -u ${var.MYSQL_ROOT_USER} -p${var.MYSQL_ROOT_PASSWORD} <<EOF
    inline = [
      <<EOT
  mysql -h ${aws_instance.db.private_ip} -u ${var.MYSQL_ROOT_USER} -p${var.MYSQL_ROOT_PASSWORD} <<EOF
  CREATE USER IF NOT EXISTS '${var.MYSQL_USER}'@'%' IDENTIFIED BY '${var.MYSQL_PASSWORD}';
  CREATE DATABASE IF NOT EXISTS petclinic;

  ALTER DATABASE petclinic
  DEFAULT CHARACTER SET utf8
  DEFAULT COLLATE utf8_general_ci;

  GRANT ALL ON petclinic.* TO '${var.MYSQL_USER}'@'%';
EOF
      EOT

    ]
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mv /home/ubuntu/${local.jar_name} /home/appuser/",
      "sudo chown -R appuser:appuser /home/appuser",
      "sudo mv /home/ubuntu/petclinic.service /lib/systemd/system/petclinic.service",
      "sudo chown root:appuser /lib/systemd/system/petclinic.service && sudo chmod o-r /lib/systemd/system/petclinic.service",
      "sudo systemctl enable petclinic.service",
      "sudo systemctl start petclinic.service"
    ]
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    host        = self.public_ip
    private_key = "${var.idrsa}"
    timeout     = "2m"
    agent       = false
  }

  tags = {
    Name = "Petclinic-APP"
  }
}


resource "aws_instance" "db" {
  ami                    = var.db_ami
  instance_type          = "t2.micro"
  key_name               = "terraform-deploy-key"
  vpc_security_group_ids = [aws_security_group.petsg-db.id]
  user_data              = templatefile("${path.root}/setupdbuser.sh", {"MYSQL_ROOT_PASSWORD" = "${var.MYSQL_ROOT_PASSWORD}", "MYSQL_ROOT_USER" = "${var.MYSQL_ROOT_USER}"})
  subnet_id              = aws_subnet.petsubnet_private.id
  tags = {
    Name = "Petclinic DB"
  }

}


output "db_privateip" {
  description = " Public ip of db instance"
  value       = aws_instance.db.private_ip
}

output "app_publicip" {
  description = " Public ip of app instance"
  value       = aws_instance.web.public_ip
}