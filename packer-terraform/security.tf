resource "aws_security_group" "petsg-app" {
  name        = "Pet-APP-SG"
  description = "Allow certain traffic"
  vpc_id      = aws_vpc.petvpc.id

  ingress {
    description = "HTTP traffic"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH traffic"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Petclinic-APP Security Group"
  }
}

resource "aws_security_group_rule" "ingress-mysql-app" {
  type                     = "ingress"
  security_group_id        = aws_security_group.petsg-app.id
  description              = "MySQL traffic"
  from_port                = 3306
  to_port                  = 3306
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.petsg-db.id
}

resource "aws_security_group" "petsg-db" {
  name        = "Pet-DB-SG"
  description = "Allow certain traffic"
  vpc_id      = aws_vpc.petvpc.id

  ingress {
    description     = "MySQL traffic"
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.petsg-app.id]
  }
  ingress {
    description     = "SSH traffic"
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [aws_security_group.petsg-app.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Petclinic-DB Security Group"
  }
}