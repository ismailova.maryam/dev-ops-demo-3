#!/bin/bash

sleep 30

sudo apt update -y
sudo apt install mysql-server -y

# Allow remote connections, since by default onlt localhost is allowed
# 0.0.0.0 means that any ip can connect. However if we change it to any specific, then it will only accept that ip
# we can configure firewall to reject other ips
sudo sed -i "s/\(^bind-address.*\)=\(.*\)/\1= 0.0.0.0/g" /etc/mysql/mysql.conf.d/mysqld.cnf 
