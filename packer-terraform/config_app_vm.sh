#!/bin/bash

# probably not the best solution
# but it seemed that packer tries to run the script earlier than required for the OS to be ready
# and if not waiting enough some packages are missing and installation of java and mysql fails
sleep 30 

sudo apt update -y
sudo apt install openjdk-11-jre mysql-client -y


sudo useradd -m -s /bin/bash -c 'APP_USER' appuser
sudo usermod -aG sudo appuser