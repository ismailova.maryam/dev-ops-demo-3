# AWS configuration variables
variable "vpc_cidr_block" {
  description = "CIDR block for VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "region" {
  description = "Region to deploy infrastructure - Frankfurt"
  type        = string
  default     = "eu-central-1"
}

variable "subnet_cidr_blocks" {
  description = "Available cidr blocks for subnets."
  type        = list(string)
  default = [
    "10.0.0.0/24",
    "10.0.1.0/24",
    "10.0.2.0/24"
  ]
}

variable "region_az" {
  description = "Availability zone ids of the region"
  type        = list(string)
  default = [
    "euc1-az1",
    "euc1-az2",
    "euc1-az3"
  ]
}

variable "java_ami" {
  description = "AMI for petclinic java server. To be provided by input"
  type        = string
}

variable "db_ami" {
  description = "AMI for petclinic java server. To be provided by input"
  type        = string
}

# variable "amis" {
#   description = "AMI based on region"
#   type        = map(string)
#   default = {
#     # Base ubuntu 20.04 focal image
#     # "eu-central-1" = "ami-0767046d1677be5a0"
#     # "eu-central-1-db" = "ami-05f7491af5eef733a"

#     # Configured ubuntu image for petclinic app
#     "eu-central-1-app" = "ami-018023b04f3b9f162"
#     "eu-central-1-db" = "ami-07752fd5c60e3c463"
#   }
# }

# Application configuration variables
variable "MYSQL_USER" {
  type        = string
  description = "MySQL database user for petclinic"
}

variable "MYSQL_PASSWORD" {
  type        = string
  description = "MySQL database user for petclinic"
}

variable "MYSQL_ROOT_USER" {
  type        = string
  description = "MySQL root username"
}

variable "MYSQL_ROOT_PASSWORD" {
  type        = string
  description = "MySQL root user password"
}

variable "idrsa" {
  type        = string
  description = "Host's private ssh key for keypair in aws"
  sensitive   = true
}