# Demo-3 for DevOps course
## Setting CI/CD for Spring Petclinic project using Gitlab CI

### Table of contents:
* [Tasks](#tasks)
* [Introduction](#introduction)
* [Flow 1](#flow-1)
* [Flow 2](#flow-2)

### Tasks


- [x] Create AWS VPC
- [x] Configure access and network groups(Security group,route table) 
- [x] 2 EC2 instances (App, DB)
- [x] Deploy jar to App ec2

- [x] A job triggered with commit pushed to repo (CI)
- [x] A job to deploy code to AWS EC2
- [x] Healthcheck step

- Additional tasks:

- [ ] Use GCP infrastructure
- [x] Deploy docker images (With a swarm cluster). [add-task-docker branch](https://gitlab.com/ismailova.maryam/dev-ops-demo-3/-/tree/add-task-docker)
- [x] Use RDS instead of EC2 instance for database. [add-task-rds branch](https://gitlab.com/ismailova.maryam/dev-ops-demo-3/-/tree/add-task-rds)
- [x] Create AMI for EC2 machines (Packer step added)


### Introduction
Demo 3 is about CI/CD using Gitlab CI and other automation tools. Starting with a commit to repository a pipeline is triggered. And depending on the branch where the commit was pushed CI/CD is run diffeently.

    Technologies used:
    
    - Gitlab
    - Packer
    - Terraform
    - Ansible
    - Docker/Swarm 

### Flow 1
The below image is relevant for the main task, and for RDS additional task.
Once pipeline is triggered three parallel processes are running.

1. Maven packages java program into a jar.

2. Packer step where AMI's for the project are searched(aws cli) and if not found they are built and stored in AWS. 

3. Terraform Steps, which initialize project and validate and later run it.
These steps wait for AMI lookup, to get the AMIs that will be used.

![Pipeline flow 1](./images/gitlabciflow-docker-Page-2.png)

Once everything is ready terraform apply is run, which initializes VPC, Security groups, EC2 instances and etc. Jar is also deployed during this stage.

In the end a healthcheck step is executed to ensure that the project is reachable and behaves as expected.

The only difference for [add-task-rds branch](https://gitlab.com/ismailova.maryam/dev-ops-demo-3/-/tree/add-task-rds) is that in AMI lookup there is no search for DB AMI, and only AMI for java is created. RDS is created using Terraform.


### Flow 2

Second flow concerns additional task for docker images.

For this task a Swarm cluster is provisionned first, where docker containers will be deployed as services inside a stack. Configuring of Swarm cluster and deployment of petclinic stack is achieved through Ansible.

Just as in [Flow 1](#flow-1) three parallel parallel process run.

1. Maven packages a jar and  then docker job builds the Dockerfile which is stored in Container Registry of the project in Gitlab

2. Swarm cluster is deployed from a an AMI which is built during this process.
An AMI is searched first, if not available it will later be built with packer and used by future jobs.

3. Terraform steps initialize project and deploy EC2 instances with installed dependencies for a swarm cluster.

    ![Pipeline flow 2](./images/gitlabciflow-docker-Page-1.png)

4. Unlike [Flow 1](#flow-1), Ansible was required here to efficiently set swarm cluster. Master and worker nodes(only 1) are created and later a stack is deployed from [docker-compose.yml](https://gitlab.com/ismailova.maryam/dev-ops-demo-3/-/blob/add-task-docker/docker-compose.yml)

In the end a healthcheck stage is run.
